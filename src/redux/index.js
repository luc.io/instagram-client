import { createStore, applyMiddleware } from 'redux'
import { Provider } from 'react-redux'

// import logger from 'redux-logger'

const localState = localStorage.getItem('ig-redux')
const initialState = localState
    ? JSON.parse(localState)
    : {
        loading: false,
        adminOrUser: 'admin',
        profiles: [],
        tag: {
            name: '',
            posts: [],
            hasNextPage: false,
            nextCursor: null
        }
    }

const reducer = (state = initialState, action) => {
    const profileIsInState = profile => state.profiles.find(p => p.username === profile)
    
    switch (action.type) {
        case 'TOGGLE_LOADING': {
            return {
                ...state,
                loading: action.payload
            }
        }
            
        case 'TOGGLE_ADMIN_AND_USER': {
            return {
                ...state,
                adminOrUser: action.payload
            }
        }
        
        case 'SET_MESSAGE': {
            return {
                ...state,
                message: action.payload
            }
        }
            
        case 'ADD_PROFILE': {
            return {
                ...state,
                rhx_gis: action.payload.rhx_gis,
                profileQueryHash: action.payload.profileQueryHash,
                profiles: profileIsInState(action.payload.profile.username)
                    ? state.profiles
                    : [
                        ...state.profiles, {
                            ...action.payload.profile,
                            addedFrom: action.payload.addedFrom
                        }
                    ]
            }
        }
        
        case 'REMOVE_PROFILE': {
            return {
                ...state,
                profiles: profileIsInState(action.payload)
                    ? state.profiles.filter(profile => profile.username !== action.payload)
                    : state.profiles
            }
        }
            
        case 'UPDATE_PROFILE': {
            return {
                ...state,
                rhx_gis: action.payload.rhx_gis,
                profileQueryHash: action.payload.profileQueryHash,
                profiles: state.profiles.map(profile =>
                    profile.username === action.payload.profile.username
                        ? {
                            ...action.payload.profile,
                            addedFrom: profile.addedFrom
                          }
                        : profile
                )
            }
        }
            
        case 'APPEND_PROFILE_POSTS': {
            return {
                ...state,
                profiles: state.profiles.map(profile =>
                    profile.username === action.payload.username
                        ? {
                            ...profile,
                            hasNextPage: action.payload.update.hasNextPage,
                            nextCursor: action.payload.update.nextCursor,
                            posts: [...profile.posts, ...action.payload.update.posts],
                            hasBeenPaged: true
                          }
                        : profile
                )
            }
        }
            
        case 'FOLLOW_PROFILE': {
            return {
                ...state,
                profiles: state.profiles.map(profile =>
                    profile.username === action.payload
                        ? {
                            ...profile,
                            addedFrom: 'manual'
                          }
                        : profile
                )
            }
        }
        
        case 'UNFOLLOW_PROFILE': {
            return {
                ...state,
                profiles: state.profiles.map(profile =>
                    profile.username === action.payload
                        ? {
                            ...profile,
                            addedFrom: 'visited'
                          }
                        : profile
                )
            }
        }
            
        case 'SAVE_TAG': {
            return {
                ...state,
                rhx_gis: action.payload.rhx_gis,
                tagQueryHash: action.payload.tagQueryHash,
                tag: { ...action.payload.tag }
            }
        }
            
        case 'APPEND_TAG_POSTS': {
            return {
                ...state,
                tag: {
                    ...state.tag,
                    hasNextPage: action.payload.hasNextPage,
                    nextCursor: action.payload.nextCursor,
                    posts: [...state.tag.posts, ...action.payload.posts]
                }
            }
        }
        
        default: break
    }
    
    return state
}

const saveToLocalStorage = store => next => action => {
    next(action)
    
    const changesToSkip = ['TOGGLE_LOADING', 'SET_MESSAGE', 'SAVE_TAG', 'APPEND_TAG_POSTS']
    if (changesToSkip.includes(action.type))
        return
    
    localStorage.setItem('ig-redux', JSON.stringify({
        ...store.getState(),
        loading: false,
        tag: {
            name: '',
            posts: [],
            hasNextPage: false,
            nextCursor: null
        }
    }))
}

const store = createStore(reducer, applyMiddleware(/* logger,  */saveToLocalStorage))

export default { Provider, store }