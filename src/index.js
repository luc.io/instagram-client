import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';

import 'semantic-ui-css/semantic.min.css'

import redux from './redux'

ReactDOM.render(
    <redux.Provider store={redux.store}>
        <App />
    </redux.Provider>,
    document.getElementById('root')
);