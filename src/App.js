import React, { Component } from 'react'
import './App.css'

import Home from './components/Home'
import Profile from './components/Profile'
import ProfileMedia from './components/ProfileMedia'
import Tags from './components/Tags'

import { Container, Message } from 'semantic-ui-react'

import { connect } from 'react-redux'

import {
    BrowserRouter as Router,
    Switch,
    Route,
    Redirect
} from 'react-router-dom'

class App extends Component {
    render() {
        return (
            <div className="App">
                <Container>
                    
                    {this.props.message
                        ? <Message
                            color="red"
                            onDismiss={() => this.props.setMessage('')}>
                            {this.props.message}
                          </Message>
                        : null}
                    
                    <Router>
                        <Switch>
                            <Route
                                exact
                                path="/"
                                render={() => <Home {...this.props} />}
                            />
                            
                            <Route
                                exact
                                path="/p/:name"
                                render={routeAttrs => <Profile
                                        routeAttrs={routeAttrs}
                                        setMessage={this.props.setMessage}
                                        queryHash={this.props.profileQueryHash}
                                        rhx_gis={this.props.rhx_gis}
                                        username={routeAttrs.match.params.name}
                                        profile={this.props.profiles.find(p => p.username === routeAttrs.match.params.name)}
                                        updateProfile={this.props.updateProfile}
                                        addProfile={this.props.addProfile}
                                        followProfile={this.props.followProfile}
                                        unfollowProfile={this.props.unfollowProfile}
                                        appendProfilePosts={this.props.appendProfilePosts}
                                    />
                                }
                            />
                            
                            <Route
                                exact
                                path="/m/:id"
                                render={routeAttrs => <ProfileMedia
                                    routeAttrs={routeAttrs}
                                    loading={this.props.loading}
                                    setLoading={this.props.setLoading}
                                    setMessage={this.props.setMessage}
                                />}
                            />
                            
                            <Route
                                path="/t/:tag?"
                                render={routeAttrs => <Tags
                                    tag={this.props.tag}
                                    routeAttrs={routeAttrs}
                                    queryHash={this.props.tagQueryHash}
                                    rhx_gis={this.props.rhx_gis}
                                    saveTag={this.props.saveTag}
                                    appendTagPosts={this.props.appendTagPosts}
                                />}
                            />
                            
                            <Route
                                render={() => {
                                    this.props.setMessage('404: Page Not Found')
                                    return <Redirect to="/" />
                                }}
                            />
                        </Switch>
                    </Router>
                    
                </Container>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        ...state
    }
}

const mapDispatchToProps = dispatch => {
    return {
        setLoading: toggle => {
            dispatch({
                type: 'TOGGLE_LOADING',
                payload: toggle
            })
        },
        
        toggleAdminAndUser: option => {
            dispatch({
                type: 'TOGGLE_ADMIN_AND_USER',
                payload: option
            })
        },
        
        setMessage: msg => {
            dispatch({
                type: 'SET_MESSAGE',
                payload: msg
            })
        },
        
        addProfile: data => {
            dispatch({
                type: 'ADD_PROFILE',
                payload: data
            })
        },
        
        removeProfile: username => {
            dispatch({
                type: 'REMOVE_PROFILE',
                payload: username
            })
        },
        
        updateProfile: updatedProfile => {
            dispatch({
                type: 'UPDATE_PROFILE',
                payload: updatedProfile
            })
        },
        
        appendProfilePosts: (username, newPosts) => {
            dispatch({
                type: 'APPEND_PROFILE_POSTS',
                payload: { username, update: newPosts }
            })
        },
        
        followProfile: username => {
            dispatch({
                type: 'FOLLOW_PROFILE',
                payload: username
            })
        },
        
        unfollowProfile: username => {
            dispatch({
                type: 'UNFOLLOW_PROFILE',
                payload: username
            })
        },
        
        saveTag: tag => {
            dispatch({
                type: 'SAVE_TAG',
                payload: tag
            })
        },
        
        appendTagPosts: newPosts => {
            dispatch({
                type: 'APPEND_TAG_POSTS',
                payload: newPosts
            })
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(App)
