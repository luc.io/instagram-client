import md5 from 'browser-md5'

const getProfile = async username => {
    const response = await fetch('https://cors.oak.re/https://instagram.com/' + username)

    if (response.status === 404) {
        throw new Error('Profile does not exist')
    }

    const text = await response.text()
    const data = getProfileData(text)

    if (data.profile.graphql.user.is_private) {
        throw new Error('Profile is private')
    }

    return {
        rhx_gis: data.rhx_gis,
        profileQueryHash: await getQueryHash(text),
        profile: { ...transformProfileData(data.profile.graphql.user) }
    }
}

const getQueryHash = async pageText => {
    const regex = /static\/bundles\/base\/ProfilePageContainer.js\/(.*).js/
    const match = regex.exec(pageText)

    const profilePageContainer = await fetch('https://cors.oak.re/https://instagram.com/' + match[0])
    const text = await profilePageContainer.text()
    const queryHashRegex = /\(r=e.profilePosts.byUserId.get\(t\)\)\|\|void 0===r\?void 0:r.pagination},queryId:"(.*?)"/
    return queryHashRegex.exec(text)[1]
}

const getProfileData = text => {
    const regex = /window\._sharedData = (.*);<\/script>/
    const match = regex.exec(text)

    if (typeof match[1] === 'undefined')
        return ''

    const json = JSON.parse(match[1])

    return {
        rhx_gis: json.rhx_gis,
        profile: { ...json.entry_data.ProfilePage[0] }
    }
}

const getNextPage = async ({ queryHash, id, howMany, cursor, rhx_gis }) => {
    const proxy = "https://cors.oak.re/"
    const host = "https://instagram.com/graphql/query/"

    const variables = `{"id":"${id}","first":${howMany},"after":"${cursor}"}`

    const gis = md5(rhx_gis + ':' + variables)

    const response = await fetch(`${proxy}${host}?query_hash=${queryHash}&variables=${encodeURIComponent(variables)}`, {
        method: "GET",
        headers: {
            "X-Instagram-GIS": gis,
            "Origin": "luc.io"
        }
    })

    if (response.status !== 200) {
        throw new Error('Something went wrong :(')
    }

    const json = await response.json()

    return transformProfileData(json.data.user.edge_owner_to_timeline_media, { fromAjax: true })
}

const transformProfileData = (raw, options = {}) => {
    if (options.fromAjax) {
        return {
            hasNextPage: raw.page_info.has_next_page,
            nextCursor: raw.page_info.end_cursor,
            posts: transformPosts(raw.edges, options)
        }
    }

    return {
        id: raw.id,
        username: raw.username,
        fullName: raw.full_name,
        bio: raw.biography,
        url: raw.external_url,
        displayPic: raw.profile_pic_url,
        numPosts: raw.edge_owner_to_timeline_media.count,
        followers: raw.edge_followed_by.count,
        following: raw.edge_follow.count,
        verified: raw.is_verified,
        hasNextPage: raw.edge_owner_to_timeline_media.page_info.has_next_page,
        nextCursor: raw.edge_owner_to_timeline_media.page_info.end_cursor,
        posts: transformPosts(raw.edge_owner_to_timeline_media.edges, options)
    }
}

const transformPosts = (rawPosts, options = {}) => {
    return rawPosts.map(rawPost => {
        rawPost = rawPost.node

        const post = {
            id: rawPost.shortcode,
            time: +rawPost.taken_at_timestamp,
            likes: options.fromAjax
                ? rawPost.edge_media_preview_like.count
                : rawPost.edge_liked_by.count,
            comments: rawPost.edge_media_to_comment.count,
            thumbnail: rawPost.thumbnail_src,
            media: rawPost.display_url,
            video: rawPost.is_video
        }

        if (rawPost.edge_media_to_caption.edges[0]) {
            post.text = rawPost.edge_media_to_caption.edges[0].node.text
        }

        return post
    })
}

export default {
    getProfile,
    getNextPage
}