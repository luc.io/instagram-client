const getPost = async id => {
    const rawPost = await retrievePost(id)
    return transformPost(rawPost)
}

const retrievePost = async id => {
    const response = await fetch('https://cors.oak.re/https://instagram.com/p/' + id)

    if (response.status === 404) {
        throw new Error('Post does not exist')
    }

    if (response.status === 200) {
        const text = await response.text()
        return getPostData(text).graphql.shortcode_media
    }
}

const getPostData = text => {
    const regex = /window\._sharedData = (.*);<\/script>/
    const match = regex.exec(text)

    if (typeof match[1] === 'undefined')
        return ''

    return JSON.parse(match[1]).entry_data.PostPage[0]
}

const transformPost = rawPost => {
    const post = {
        id: rawPost.shortcode,
        time: +rawPost.taken_at_timestamp,
        likes: rawPost.edge_media_preview_like.count,
        comments: rawPost.edge_media_to_comment.count,
        thumbnail: rawPost.thumbnail_src,
        media: rawPost.display_url,
        user: {
            displayPic: rawPost.owner.profile_pic_url,
            username: rawPost.owner.username
        }
    }

    if (rawPost.edge_media_to_caption.edges[0])
        post.text = rawPost.edge_media_to_caption.edges[0].node.text

    if (rawPost.is_video)
        post.video = rawPost.video_url

    return post
}

export default {
    getPost
}