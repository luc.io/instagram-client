import Profile from './profile'
import Post from './post'
import Tags from './tags'

export default {
    Profile, Post, Tags
}