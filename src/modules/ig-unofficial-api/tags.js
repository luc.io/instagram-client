import md5 from 'browser-md5'

const getTag = async tag => {
    const response = await fetch('https://cors.oak.re/https://instagram.com/explore/tags/' + tag)
    
    const text = await response.text()
    const data = getTagData(text)

    return {
        rhx_gis: data.rhx_gis,
        tagQueryHash: await getQueryHash(text),
        tag: {
            name: data.graphql.hashtag.name,
            posts: [...transformTagPosts(data.graphql.hashtag.edge_hashtag_to_media.edges)],
            hasNextPage: data.graphql.hashtag.edge_hashtag_to_media.page_info.has_next_page,
            nextCursor: data.graphql.hashtag.edge_hashtag_to_media.page_info.end_cursor
        }
    }
}

const getTagData = text => {
    const regex = /window\._sharedData = (.*);<\/script>/
    const match = regex.exec(text)

    if (typeof match[1] === 'undefined')
        return ''

    const json = JSON.parse(match[1])

    return {
        ...json.entry_data.TagPage[0],
        rhx_gis: json.rhx_gis
    }
}

const transformTagPosts = rawPosts => {
    return rawPosts.map(rawPost => {
        rawPost = rawPost.node

        const post = {
            id: rawPost.shortcode,
            time: +rawPost.taken_at_timestamp,
            likes: rawPost.edge_liked_by.count,
            comments: rawPost.edge_media_to_comment.count,
            thumbnail: rawPost.thumbnail_src,
            media: rawPost.display_url,
            video: rawPost.is_video
        }

        if (rawPost.edge_media_to_caption.edges[0]) {
            post.text = rawPost.edge_media_to_caption.edges[0].node.text
        }

        return post
    })
}

const getQueryHash = async pageText => {
    const regex = /static\/bundles\/base\/TagPageContainer.js\/(.*).js/
    const match = regex.exec(pageText)

    const tagPageContainer = await fetch('https://cors.oak.re/https://instagram.com/' + match[0])
    const text = await tagPageContainer.text()
    const queryHashRegex = /e.tagMedia.byTagName.get\(t\)\);return n===A.d\?o.topPagination:o.pagination},queryId:"(.*?)"/
    return queryHashRegex.exec(text)[1]
}

const getNextPage = async ({queryHash, tagName, howMany, cursor, rhx_gis}) => {
    const proxy = "https://cors.oak.re/"
    const host = "https://instagram.com/graphql/query/"

    const variables = `{"tag_name":"${tagName}","first":${howMany},"after":"${cursor}"}`

    const gis = md5(rhx_gis + ':' + variables)

    const response = await fetch(`${proxy}${host}?query_hash=${queryHash}&variables=${encodeURIComponent(variables)}`, {
        method: "GET",
        headers: {
            "X-Instagram-GIS": gis,
            "Origin": "luc.io"
        }
    })

    if (response.status !== 200) {
        throw new Error('Something went wrong :(')
    }

    const json = await response.json()
    
    return transformTagData(json.data.hashtag.edge_hashtag_to_media)
}

const transformTagData = raw => {
    return {
        hasNextPage: raw.page_info.has_next_page,
        nextCursor: raw.page_info.end_cursor,
        posts: transformPosts(raw.edges)
    }
}

const transformPosts = rawPosts => {
    return rawPosts.map(rawPost => {
        rawPost = rawPost.node

        const post = {
            id: rawPost.shortcode,
            time: +rawPost.taken_at_timestamp,
            likes: rawPost.edge_liked_by.count,
            comments: rawPost.edge_media_to_comment.count,
            thumbnail: rawPost.thumbnail_src,
            media: rawPost.display_url,
            video: rawPost.is_video
        }

        if (rawPost.edge_media_to_caption.edges[0]) {
            post.text = rawPost.edge_media_to_caption.edges[0].node.text
        }

        return post
    })
}

export default {
    getTag, getNextPage
}