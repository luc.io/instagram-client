import React, { Component } from 'react'

import { Link } from 'react-router-dom'

import ProfileGalleryPost from '../Profile/ProfileGallery/ProfileGalleryPost'
import Aux from '../HOC/Aux'

import { Menu, Loader, Dimmer } from 'semantic-ui-react'

import instagram from '../../modules/ig-unofficial-api'

class ProfileMedia extends Component {
    state = {
        refreshing: false
    }
    
    async componentDidMount() {
        await this.loadAndSavePost()
        
        if (!this.state.post)
            return

        const newTitle = 'IG | post by @' + this.state.post.user.username
        if (document.title !== newTitle)
            document.title = newTitle
    }
    
    loadAndSavePost = async () => {
        try {
            this.setState({ refreshing: true })
            
            const post = await instagram.Post.getPost(this.props.routeAttrs.match.params.id)
            return this.setState({ post, refreshing: false })
        } catch (e) {
            this.props.setMessage(e.message)
            return this.setState({ refreshing: false })
        }
    }
    
    goBack = () => this.props.routeAttrs.history.goBack()
    
    render() {
        if (!this.state.post)
            return <Loader active />
        
        return (
            <Aux>
                <Dimmer active={this.state.refreshing}>
                    <Loader />
                </Dimmer>
                
                <Menu secondary fixed="top" style={{
                    WebkitBackdropFilter: 'blur(5px)',
                    borderBottom: '1px solid rgba(0, 0, 0, 0.07)'
                }}>
                    <Menu.Menu position="left" style={{ zIndex: 1 }}>
                        <Menu.Item
                            onClick={this.goBack}
                            name=""
                            icon="chevron left"
                        />
                        <Menu.Item
                            as={Link}
                            to="/t/"
                            icon="hashtag"
                        />
                    </Menu.Menu>
        
                    <div style={{
                        position: "absolute",
                        right: 0,
                        left: 0,
                        textAlign: "center",
                        paddingTop: 10,
                        fontWeight: "bold"
                    }}>
                        {this.state.post.video ? 'Video' : 'Photo'}
                    </div>
                        
                    <Menu.Menu position="right" style={{ zIndex: 1 }}>
                        <Menu.Item
                            name=""
                            icon="refresh"
                            onClick={this.loadAndSavePost}
                        />
                    </Menu.Menu>
                </Menu>
                    
                <ProfileGalleryPost
                    customStyle={{
                        marginLeft: -14,
                        marginRight: -14
                    }}
                    post={this.state.post}
                    layout="linear"
                    displayPic={this.state.post.user.displayPic}
                    username={this.state.post.user.username}
                />
            </Aux>
        )
    }
}

export default ProfileMedia