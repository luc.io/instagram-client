import React from 'react'

import { Link } from 'react-router-dom'

import Aux from '../HOC/Aux'
import AddProfileInput from '../AddProfileInput'
import ProfileList from '../ProfileList'

import { Menu, Image } from 'semantic-ui-react'

const home = props => {
    
    const newTitle = 'Instagram'
    if (document.title !== newTitle)
        document.title = newTitle
        
    return (
        <Aux>
            <Menu secondary fixed="top" style={{
                WebkitBackdropFilter: 'blur(5px)',
                borderBottom: '1px solid rgba(0, 0, 0, 0.07)'
            }}>
                <Menu.Menu position="left">
                    <Menu.Item
                        as={Link}
                        to="/t/"
                        icon="hashtag"
                    />
                </Menu.Menu>
                
                <Image style={{
                    width: '100px',
                    position: 'absolute',
                    left: 'calc(50% - 50px)',
                    pointerEvents: 'none',
                    paddingTop: '3px'
                }} src="/ig-logo-1.png" />
                
                <Menu.Menu position="right">
                    <Menu.Item
                        icon="cog"
                        onClick={() => props.toggleAdminAndUser('admin')}
                        active={props.adminOrUser === 'admin'} />
                    <Menu.Item
                        icon='user'
                        onClick={() => props.toggleAdminAndUser('user')}
                        active={props.adminOrUser === 'user'} />
                </Menu.Menu>
            </Menu>

            {props.adminOrUser === 'admin'
                ? <AddProfileInput
                    setMessage={props.setMessage}
                    setLoading={props.setLoading}
                    loading={props.loading}
                    addProfile={props.addProfile}
                  />
                : null}
    
            <ProfileList
                profiles={props.profiles}
                followProfile={props.followProfile}
                removeProfile={props.removeProfile}
                admin={props.adminOrUser === 'admin'}
            />
        </Aux>
    )
}

export default home