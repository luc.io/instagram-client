import React from 'react'

import Aux from '../HOC/Aux'

import { List, Grid } from 'semantic-ui-react'

import ProfileItem from './ProfileItem'

const profileList = props => {
    if (props.profiles.length === 0)
        return <h2 style={{
            textAlign: 'center',
            textTransform: 'uppercase',
            fontSize: '1.4em',
            letterSpacing: '1px',
            paddingTop: props.admin ? 0 : 40
        }}>No Profiles</h2>
    
    const addedManually = props.profiles.filter(profile => profile.addedFrom === 'manual')
    const visited = props.profiles.filter(profile => profile.addedFrom === 'visited')
    
    if (props.admin) {
        return (
            <Aux>
                <div style={{
                    display: addedManually.length
                        ? 'block'
                        : 'none',
                    marginTop: 30
                }}>
                    <h2 style={{
                        textAlign: 'center',
                        textTransform: 'uppercase',
                        fontSize: '1.4em',
                        letterSpacing: '1px'
                    }}>Following</h2>
                    <List verticalAlign='middle' relaxed>
                        {addedManually.map(profile =>
                            <ProfileItem
                                key={profile.username}
                                profile={profile}
                                removeProfile={props.removeProfile}
                                admin={props.admin}
                            />
                        )}
                    </List>
                </div>
            
                <div style={{
                    display: visited.length
                        ? 'block'
                        : 'none',
                    marginTop: 30
                }}>
                    <h2 style={{
                        textAlign: 'center',
                        textTransform: 'uppercase',
                        fontSize: '1.4em',
                        letterSpacing: '1px'
                    }}>Visited</h2>
                    <List verticalAlign='middle' relaxed>
                        {visited.map(profile =>
                            <ProfileItem
                                key={profile.username}
                                profile={profile}
                                followProfile={props.followProfile}
                                removeProfile={props.removeProfile}
                                admin={props.admin}
                            />
                        )}
                    </List>
                </div>
            </Aux>
        )
    }
    
    return (
        <Aux>
            <div style={{
                display: addedManually.length
                    ? 'block'
                    : 'none',
                paddingTop: 20
            }}>
                <h2 style={{
                    textAlign: 'center',
                    textTransform: 'uppercase',
                    fontSize: '1.4em',
                    letterSpacing: '1px'
                }}>Following</h2>
                <Grid columns={2} relaxed style={{ paddingTop: 0 }}>
                    {addedManually.map(profile =>
                        <ProfileItem
                            key={profile.username}
                            profile={profile}
                            admin={props.admin}
                        />
                    )}
                </Grid>
            </div>
            
            <div style={{ display: visited.length ? 'block' : 'none', paddingTop: 20 }}>
                <h2 style={{
                    textAlign: 'center',
                    textTransform: 'uppercase',
                    fontSize: '1.4em',
                    letterSpacing: '1px',
                    paddingTop: 40
                }}>Visited</h2>
                <Grid columns={2} relaxed style={{ paddingTop: 0 }}>
                    {visited.map(profile =>
                        <ProfileItem
                            key={profile.username}
                            profile={profile}
                            admin={props.admin}
                        />
                    )}
                </Grid>
            </div>
        </Aux>
    )
}

export default profileList