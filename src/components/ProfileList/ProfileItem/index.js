import React from 'react'

import './index.css'

import { Image, Button, List, Grid, Icon } from 'semantic-ui-react'

import { Link } from 'react-router-dom'

const profileItem = props => {
    const adminView = (
        <List.Item as={Link} to={"/p/" + props.profile.username + '?from-home=true'}>
            <Image avatar src={props.profile.displayPic} />
            <List.Content>{props.profile.username}</List.Content>

            <List.Content floated='right'>
                <Button
                    size='mini'
                    basic
                    color='green'
                    style={{
                        display: props.profile.addedFrom === 'manual'
                            ? 'none'
                            : 'initial'
                    }}
                    onClick={e => {
                        e.preventDefault()
                        props.followProfile(props.profile.username)
                    }}>
                    <Icon
                        color="green"
                        name="check"
                        style={{
                            padding: 0,
                            margin: 0
                        }} />
                </Button>
                
                <Button
                    size='mini'
                    basic
                    color='red'
                    onClick={e => {
                        e.preventDefault()
                        props.removeProfile(props.profile.username)
                    }}>
                    <Icon
                        color="red"
                        name="close"
                        style={{
                            padding: 0,
                            margin: 0
                        }} />
                </Button>
            </List.Content>
        </List.Item>
    )
    
    const userView = (
        <Grid.Column className="profile-item-user">
            <Link to={"/p/" + props.profile.username + '?from-home=true'}>
                <Image avatar src={props.profile.displayPic} />
                <span className="profile-name">{props.profile.username}</span>
                
                <Grid columns={2} style={{ paddingTop: 25 }}>
                    {props.profile.posts
                        .slice(0, 4)
                        .map(post => (
                            <Grid.Column key={post.id} style={{ padding: 0 }}>
                                <div>
                                    <Image src={post.thumbnail} />
                                    <Icon name="play" size="large" inverted style={{
                                        display: post.video ? 'block' : 'none',
                                        position: 'absolute',
                                        zIndex: 1,
                                        right: 5,
                                        top: 10,
                                        pointerEvents: 'none',
                                        textShadow: 'rgb(150, 150, 150) 1px 1px 5px'
                                    }} />
                                </div>
                            </Grid.Column>
                        ))}
                </Grid>
            </Link>
        </Grid.Column>
    )
    
    return props.admin
        ? adminView
        : userView
}

export default profileItem