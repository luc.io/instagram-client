import React, { Component } from 'react'

import { Input } from 'semantic-ui-react'

import instagram from '../../modules/ig-unofficial-api'

class AddProfileInput extends Component {
    state = {
        profileError: false,
        inputValue: ''
    }
    
    addProfile = async e => {
        if (e.key !== 'Enter')
            return

        const { value } = e.target

        if (!value)
            return

        this.props.setLoading(true)
        
        try {
            const data = await instagram.Profile.getProfile(value)

            this.props.addProfile({
                ...data, addedFrom: 'manual'
            })
            
            this.setState({ inputValue: '' })
        } catch (e) {
            this.props.setMessage(e.message)
            this.setState({ profileError: true })
        }
        
        return this.props.setLoading(false)
    }

    inputChange = e => {
        if (this.state.profileError) {
            this.props.setMessage('')
            this.setState({ profileError: false })
        }

        if (this.state.inputValue !== e.target.value) {
            this.setState({ inputValue: e.target.value })
        }
    }

    render() {
        return <Input
            fluid
            size="large"
            loading={this.props.loading}
            disabled={this.props.loading}
            error={this.state.profileError}
            icon="user"
            placeholder="Add Profile..."
            type="text"
            autoComplete="off"
            value={this.state.inputValue}
            onKeyPress={this.addProfile}
            onChange={this.inputChange}
        />
    }
}

export default AddProfileInput