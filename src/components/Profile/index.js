import React, { Component } from 'react'

import { Link, Redirect } from 'react-router-dom'

import Aux from '../HOC/Aux'
import ProfileGallery from './ProfileGallery'
import UserMeta from './UserMeta'

import { Menu, Loader, Dimmer, Button } from 'semantic-ui-react'

import instagram from '../../modules/ig-unofficial-api'

class Profile extends Component {
    state = {
        refreshing: false,
        loadingMore: false
    }
    
    async componentDidMount() {
        setTimeout(() => {
            const hash = window.location.hash
            if (!hash) return
            
            document
                .getElementById(hash.replace('#', ''))
                .scrollIntoView()
        }, 0)
        
        try {
            if (this.props.profile) {
                if (this.props.profile.hasBeenPaged)
                    return

                return (await this.reloadProfile())
            }

            this.setState({ refreshing: true })

            const data = await instagram.Profile.getProfile(this.props.username)

            this.props.addProfile({
                ...data, addedFrom: 'visited'
            })

            return this.setState({
                refreshing: false,
                loadingMore: false
            })
        } catch (e) {
            this.props.setMessage(e.message)
            this.setState({ profileError: true })
            return this.setState({ redirectToError: true })
        }
    }
    
    componentDidUpdate() {
        if (this.state.redirectToError)
            return
        
        if (!this.props.profile)
            return
        
        const newTitle = 'IG | @' + this.props.profile.username
        if (document.title !== newTitle)
            document.title = newTitle
    }
    
    reloadProfile = async () => {
        this.setState({ refreshing: true })
        
        const data = await instagram.Profile.getProfile(this.props.profile.username)
        this.setState({ refreshing: false })
        return this.props.updateProfile(data)
    }
    
    loadMore = async () => {
        this.setState({ loadingMore: true })
        
        const nextPage = await instagram.Profile.getNextPage({
            rhx_gis: this.props.rhx_gis,
            queryHash: this.props.queryHash,
            cursor: this.props.profile.nextCursor,
            hasNextPage: this.props.profile.hasNextPage,
            id: this.props.profile.id,
            howMany: 18
        })
        
        this.props.appendProfilePosts(this.props.profile.username, nextPage)
        
        this.setState({ loadingMore: false })
    }
    
    render() {
        if (this.state.redirectToError)
            return <Redirect to='/' />
        
        if (!this.props.profile)
            return <Loader active />
        
        const user = this.props.profile
        
        const loadMoreButton = (
            <Aux>
                <Button
                    fluid
                    disabled={this.state.loadingMore}
                    loading={this.state.loadingMore}
                    onClick={this.loadMore}
                    style={{ marginTop: 35 }}>
                    Load More
                </Button>
                <br />
                <br />
                <br />
            </Aux>
        )
        
        const params = new URLSearchParams(this.props.routeAttrs.location.search)
        const fromHome = params.get('from-home')
        
        return (
            <Aux>
                <Dimmer active={this.state.refreshing}>
                    <Loader />
                </Dimmer>
                
                <Menu secondary fixed="top" style={{
                    WebkitBackdropFilter: 'blur(5px)',
                    borderBottom: '1px solid rgba(0, 0, 0, 0.07)'
                }}>
                    <Menu.Menu position="left" style={{ zIndex: 1 }}>
                        {fromHome
                            ? <Menu.Item as={Link} to="/" name="" icon="home" />
                            : <Menu.Item
                                onClick={this.props.routeAttrs.history.goBack}
                                name=""
                                icon="chevron left"
                            />}
                        <Menu.Item
                            as={Link}
                            to="/t/"
                            icon="hashtag"
                        />
                    </Menu.Menu>
                    
                    <div style={{
                        position: "absolute",
                        right: 0,
                        left: 0,
                        textAlign: "center",
                        paddingTop: 10,
                        fontWeight: "bold"
                    }}>{user.username}</div>
                    
                    <Menu.Menu position="right" style={{ zIndex: 1 }}>
                        <Menu.Item
                            name=""
                            icon="refresh"
                            onClick={this.reloadProfile}
                        />
                    </Menu.Menu>
                </Menu>
                
                <UserMeta
                    user={user}
                    addedFrom={this.props.profile.addedFrom}
                    followProfile={() => this.props.followProfile(user.username)}
                    unfollowProfile={() => this.props.unfollowProfile(user.username)}
                />
                
                <ProfileGallery
                    displayPic={user.displayPic}
                    username={user.username}
                    posts={user.posts}
                    goTo={this.props.routeAttrs.history.replace}
                />
                
                {this.props.profile.hasNextPage
                    ? loadMoreButton
                    : null}
            </Aux>
        )
    }
}

export default Profile