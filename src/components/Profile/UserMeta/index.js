import React from 'react'

import UserMetaHeader from './UserMetaHeader'
import UserMetaDescription from './UserMetaDescription'

import { Grid } from 'semantic-ui-react'

const userMeta = props => {
    return (
        <Grid style={{ paddingTop: 4 }}>
            <UserMetaHeader
                user={props.user}
                addedFrom={props.addedFrom}
                followProfile={props.followProfile}
                unfollowProfile={props.unfollowProfile}
            />
            <UserMetaDescription user={props.user} />
        </Grid>
    )
}

export default userMeta