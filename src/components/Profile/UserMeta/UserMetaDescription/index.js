import React from 'react'

import { Grid, Icon } from 'semantic-ui-react'

const userMetaDescription = props => {
    const verifiedStar = (
        <Icon.Group style={{marginLeft: 5}}>
            <Icon name='certificate' color="blue" />
            <Icon name='check' inverted style={{
                fontSize: '0.5em',
                marginLeft: '-1.8px'
            }} />
        </Icon.Group>
    )
    
    return (
        <Grid.Row>
            <Grid.Column>
                <div style={{ fontSize: '1.1em' }}>
                    <strong>
                        {props.user.fullName}
                        {props.user.verified
                            ? verifiedStar
                            : null}
                    </strong>
                </div>
                
                <div style={{
                    whiteSpace: 'pre-line'
                }}>{props.user.bio}</div>
                
                <div style={{
                    overflow: 'hidden',
                    maxWidth: '80%',
                    textOverflow: 'ellipsis'
                }}>
                    {props.user.url
                        ? <a href={props.user.url} target="_blank" rel="noopener noreferrer">
                            {props.user.url.replace(/http:\/\/www.|https:\/\/www.|http:\/\/|https:\/\//, '')}
                          </a>
                        : null}
                </div>
            </Grid.Column>
        </Grid.Row>
    )
}

export default userMetaDescription