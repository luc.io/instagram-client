import React from 'react'

import UserMetaHeaderStats from './UserMetaHeaderStats'

import { Grid, Image, Button, Icon } from 'semantic-ui-react'

const userMetaHeader = props => {
    const isFollowed = props.addedFrom === 'manual'
    
    const followButton = (
        <Button
            fluid
            color="blue"
            size="mini"
            onClick={props.followProfile}
            style={{ textTransform: 'uppercase', marginTop: 10 }}>
            Follow
        </Button>
    )
    
    const followingButton = (
        <Button
            fluid
            size="mini"
            onClick={props.unfollowProfile}
            style={{ textTransform: 'uppercase', marginTop: 10 }}>
            Following&nbsp;&nbsp;&nbsp;
            <Icon name="check" />
        </Button>
    )
    
    return (
        <Grid.Row>
            <Grid.Column width={5} style={{ paddingRight: 7 }}>
                <Image
                    circular
                    src={props.user.displayPic}
                    style={{ border: '1px solid lightgrey' }}
                />
            </Grid.Column>

            <Grid.Column width={11} style={{ paddingLeft: 7 }}>
                <Grid.Row>
                    <UserMetaHeaderStats user={props.user} />
                </Grid.Row>

                {isFollowed
                    ? followingButton
                    : followButton}
            </Grid.Column>
        </Grid.Row>
    )
}

export default userMetaHeader