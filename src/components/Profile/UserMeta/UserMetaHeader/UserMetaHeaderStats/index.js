import React from 'react'

import { Grid } from 'semantic-ui-react'

const userMetaHeaderStats = props => {
    const format = num => {
        if (num >= 1000000000)
            return (num / 1000000000).toFixed(1).replace(/\.0$/, '') + 'B'
        
        if (num >= 1000000)
            return (num / 1000000).toFixed(1).replace(/\.0$/, '') + 'M'
        
        if (num >= 10000)
            return (num / 1000).toFixed(1).replace(/\.0$/, '') + 'K'

        return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
    }
    
    const stats = [
        { label: 'posts', value: format(props.user.numPosts) },
        { label: 'followers', value: format(props.user.followers) },
        { label: 'following', value: format(props.user.following) },
    ]
    
    return (
        <Grid columns={stats.length} style={{ textAlign: 'center', paddingBottom: 5 }}>
            {stats.map(stat => (
                <Grid.Column key={stat.label}>
                    <div>
                        <strong style={{ fontSize: '1.15em' }}>{stat.value}</strong>
                        <br />
                        <span style={{
                            fontSize: '0.9em',
                            color: 'grey'
                        }}>
                            {stat.label}
                        </span>
                    </div>
                </Grid.Column>
            ))}
        </Grid>
    )
}

export default userMetaHeaderStats