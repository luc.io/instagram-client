import React, { Component } from 'react'

import ProfileGalleryPost from './ProfileGalleryPost'
import Aux from '../../HOC/Aux'

import { Grid, Icon } from 'semantic-ui-react'

class ProfileGallery extends Component {
    state = {
        rowLength: 3
    }
    
    updateRowLength = x => {
        if (this.state.rowLength === x)
            return
        
        this.setState({ rowLength: x })
    }
    
    render() {
        return (
            <Aux>
                <Grid columns={2} style={{
                    textAlign: 'center',
                    borderTop: '1px solid rgba(0, 0, 0, 0.07)'
                }}>
                    <Grid.Column onClick={() => this.updateRowLength(3)}>
                        <Icon size="large" name="grid layout" />
                    </Grid.Column>
                
                    <Grid.Column onClick={() => this.updateRowLength(1)}>
                        <Icon size="large" name="image" />
                    </Grid.Column>
                </Grid>
            
                <Grid columns={this.state.rowLength} style={{ paddingTop: 0 }}>
                    {this.props.posts.map(post => <ProfileGalleryPost
                        key={post.id}
                        post={post}
                        layout={this.state.rowLength === 1 ? 'linear' : 'grid'}
                        displayPic={this.props.displayPic}
                        username={this.props.username}
                        goTo={this.props.goTo}
                    />)}
                </Grid>
            </Aux>
        )
    }
}

export default ProfileGallery