import React from 'react'

import { Grid, Image } from 'semantic-ui-react'

import { Link } from 'react-router-dom'

const profileGalleryPostHead = props => {
    return (
        <Grid.Row hidden={props.layout === 'grid'} style={{ padding: 10 }}>
            <Link to={"/p/" + props.username} style={{
                color: 'black'
            }}>
                <Image
                    avatar
                    size="mini"
                    src={props.displayPic}
                    style={{ border: '1px solid lightgrey' }}
                />
            
                <span style={{
                    paddingLeft: 10,
                    fontSize: '1.1em',
                    transform: 'translateY(1px)',
                    display: 'inline-block'
                }}>
                    <strong>{props.username}</strong>
                </span>
            </Link>
        </Grid.Row>
    )
}

export default profileGalleryPostHead