import React from 'react'

import './index.css'

import ProfileGalleryPostHead from './ProfileGalleryPostHead'
import ProfileGalleryPostFoot from './ProfileGalleryPostFoot'

import { Grid, Image, Icon } from 'semantic-ui-react'

const profileGalleryPost = props => {
    const gridImage = (
        <div id={props.post.id}>
            <Image
                onClick={() => {
                    window.location.hash = props.post.id
                    props.goTo('/m/' + props.post.id)
                }}
                src={props.post.thumbnail}
            />

            <Icon name="play" size="large" inverted style={{
                display: props.post.video ? 'block' : 'none',
                position: 'absolute',
                zIndex: 1,
                right: 5,
                top: 10,
                pointerEvents: 'none',
                textShadow: 'rgb(150, 150, 150) 1px 1px 5px'
            }} />
        </div>
    )
    
    const linearImage = typeof props.post.video === 'string'
        ? (
            <Image
                id={props.post.id}
                src={props.post.media}
            />
        )
        : (
            <div id={props.post.id} style={{ position: 'relative' }}>
                <Image
                    onClick={() => {
                        window.location.hash = props.post.id
                        props.goTo('/m/' + props.post.id)
                    }}
                    src={props.post.media}
                />

                <Icon name="play" size="large" inverted style={{
                    display: props.post.video ? 'block' : 'none',
                    position: 'absolute',
                    zIndex: 1,
                    width: '100%',
                    top: 'calc(50% - 10px)',
                    pointerEvents: 'none',
                    textShadow: 'rgb(150, 150, 150) 1px 1px 5px'
                }} />
            </div>
        )
    
    const video = (
        <video
            width="100%"
            height="auto"
            poster={props.post.media}
            muted
            loop
            autoPlay
            playsInline
            controls>
            <source
                src={props.post.video}
                type="video/mp4" />
            {linearImage}
        </video>
    )
    
    return (
        <Grid.Column
            className={"gallery-image" + (props.layout === 'grid' ? ' grid-layout' : '')}
            style={props.customStyle || {}}>
            
            <ProfileGalleryPostHead
                layout={props.layout}
                displayPic={props.displayPic}
                username={props.username}
            />
            
            {props.layout === 'grid'
                ? gridImage
                : typeof props.post.video === 'string'
                    ? video
                    : linearImage}
            
            <ProfileGalleryPostFoot
                layout={props.layout}
                post={props.post}
                username={props.username} />
        </Grid.Column>
    )
}

export default profileGalleryPost