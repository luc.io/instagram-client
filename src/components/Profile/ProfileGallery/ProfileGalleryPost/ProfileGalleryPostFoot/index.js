import React from 'react'

import { Link } from 'react-router-dom'

import { Grid, Icon } from 'semantic-ui-react'

const profileGalleryPostFoot = props => {
    const getDate = unixTime => {
        const date = new Date(unixTime * 1000)

        return date.toLocaleDateString([], { month: 'long' })
            + ' ' + date.getDate()
            + ', ' + date.getFullYear()
    }
    
    const addCommas = num => num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
    
    const parseTagsAndAts = text =>
        text.split(/([#|＃|@][^\s\W]+)/g).map(word => {
            if (word.startsWith('#'))
                return <Link to={"/t/" + word.slice(1)}>{word}</Link>
                
            if (word.startsWith('@'))
                return <Link to={"/p/" + word.slice(1)}>{word}</Link>
                
            return <span>{word}</span>
        })
    
    return (
        <Grid.Row hidden={props.layout === 'grid'} style={{ padding: 10 }}>
            <Icon size="big" name="like outline" />
            <Icon size="big" name="comment outline" style={{ marginLeft: 10, marginRight: 10 }} />
            <Icon size="big" name="send outline" />
            <Icon size="big" name="bookmark outline" style={{ float: 'right' }} />

            <div style={{ paddingTop: 10 }}>
                <strong>
                    {props.post.likes === 0
                        ? 'No likes'
                        : addCommas(props.post.likes) + (props.post.likes === 1 ? ' like' : ' likes')}
                </strong>
            </div>

            {props.post.text
                ? <div style={{ paddingTop: 5 }}>
                    <Link to={"/p/" + props.username} style={{ color: 'black' }}>
                        <strong style={{ paddingRight: 4 }}>
                            {props.username}
                        </strong>
                    </Link>
                    <span style={{
                        whiteSpace: 'pre-line'
                    }}>{parseTagsAndAts(props.post.text)}</span>
                </div>
                : null}

            {props.post.comments > 0
                ? <div style={{ color: 'grey', paddingTop: 5 }}>{props.post.comments === 1
                    ? 'View 1 comment'
                    : `View all ${props.post.comments} comments`}
                </div>
                : null}

            <div style={{ textTransform: 'uppercase', fontSize: '0.8em', color: 'grey', paddingTop: 5 }}>
                {getDate(props.post.time)}
            </div>
        </Grid.Row>
    )
}

export default profileGalleryPostFoot