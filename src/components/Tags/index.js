import React, { Component } from 'react'

import './index.css'

import Aux from '../HOC/Aux'

import { Link } from 'react-router-dom'

import {
    Menu,
    Input,
    Image,
    Grid,
    Button,
    Icon,
    Dimmer,
    Loader
} from 'semantic-ui-react'

import instagram from '../../modules/ig-unofficial-api'

class Tags extends Component {
    state = {
        refreshing: false,
        loadingMore: false
    }
    
    async componentDidMount() {
        setTimeout(() => {
            const hash = window.location.hash
            if (!hash) return

            if (!this.props.tag.name)
                return
            
            document
                .getElementById(hash.replace('#', ''))
                .scrollIntoView()
        }, 0)
        
        /* When clicking the # button, which goes to /t/ and there is
        already a tag loaded, add the tag to the URL */
        if (this.props.tag.name && !this.props.routeAttrs.match.params.tag)
            return this.props.routeAttrs.history.replace('/t/' + this.props.tag.name)
        
        /* When there is not a tag already loaded, but there is a tag in
        the URL it means we should load the tag */
        if (!this.props.tag.name && this.props.routeAttrs.match.params.tag)
            await this.reloadTag(this.props.routeAttrs.match.params.tag)
            
        /* If there is a tag loaded but we are trying to access a different one
        then load the new tag */
        if (this.props.tag.name && this.props.routeAttrs.match.params.tag
            && (this.props.tag.name !== this.props.routeAttrs.match.params.tag))
            await this.reloadTag(this.props.routeAttrs.match.params.tag)
    }
    
    componentDidUpdate() {
        const newTitle = this.props.tag.name
            ? 'IG | #' + this.props.tag.name
            : 'Instagram'
        
        if (document.title !== newTitle)
            document.title = newTitle
    }
    
    reloadTag = async tagName => {
        this.setState({ refreshing: true })

        const tag = await instagram.Tags.getTag(tagName)
        this.props.saveTag(tag)
        
        this.setState({ refreshing: false })
    }

    search = async e => {
        if (e.key !== 'Enter')
            return
        
        if (!e.target.value)
            return
            
        this.props.routeAttrs.history.push('/t/' + e.target.value)
        await this.reloadTag(e.target.value)
    }
    
    loadMore = async () => {
        this.setState({ loadingMore: true })

        const nextPage = await instagram.Tags.getNextPage({
            rhx_gis: this.props.rhx_gis,
            queryHash: this.props.queryHash,
            cursor: this.props.tag.nextCursor,
            hasNextPage: this.props.tag.hasNextPage,
            tagName: this.props.tag.name,
            howMany: 18
        })

        this.props.appendTagPosts(nextPage)
        return this.setState({ loadingMore: false })
    }
    
    render() {
        const posts = this.props.tag.posts.length
            ? (
                <Grid columns={3}>
                    {this.props.tag.posts.map(post => {
                        return (
                            <Grid.Column
                                id={post.id}
                                className="tag-post"
                                key={post.id}
                                onClick={() => {
                                    window.location.hash = post.id
                                    this.props.routeAttrs.history.replace('/m/' + post.id)
                                }}>
                                <Image src={post.thumbnail} />
                                
                                <Icon name="play" size="large" inverted style={{
                                    display: post.video ? 'block' : 'none',
                                    position: 'absolute',
                                    zIndex: 1,
                                    right: 5,
                                    top: 10,
                                    pointerEvents: 'none',
                                    textShadow: 'rgb(150, 150, 150) 1px 1px 5px'
                                }} />
                            </Grid.Column>
                        )
                    })}
                </Grid>
            )
            : null
        
        const loadMoreButton = (
            <Aux>
                <Button
                    fluid
                    disabled={this.state.loadingMore}
                    loading={this.state.loadingMore}
                    onClick={this.loadMore}
                    style={{ marginTop: 35 }}>
                    Load More
                </Button>
                <br />
                <br />
                <br />
            </Aux>
        )
            
        return (
            <Aux>
                <Dimmer active={this.state.refreshing}>
                    <Loader />
                </Dimmer>
                
                <Menu secondary fixed="top" style={{
                    WebkitBackdropFilter: 'blur(5px)',
                    borderBottom: '1px solid rgba(0, 0, 0, 0.07)'
                }}>
                    <Menu.Menu position="left" style={{ zIndex: 1 }}>
                        <Menu.Item
                            as={Link}
                            to="/"
                            name=""
                            icon="home"
                        />
                    </Menu.Menu>

                    <div style={{
                        position: "absolute",
                        right: 0,
                        left: 0,
                        textAlign: "center",
                        paddingTop: 10,
                        fontWeight: "bold"
                    }}>
                        {this.props.tag.name
                            ? "#" + this.props.tag.name
                            : "Hashtags"}
                    </div>
                    
                    <Menu.Menu position="right" style={{
                        zIndex: 1,
                        display: this.props.tag.name ? 'block' : 'none'
                    }}>
                        <Menu.Item
                            icon="refresh"
                            onClick={() => this.reloadTag(this.props.tag.name)}
                        />
                    </Menu.Menu>
                </Menu>
        
                <Input
                    fluid
                    size="large"
                    loading={this.state.refreshing}
                    disabled={this.state.refreshing}
                    icon="hashtag"
                    placeholder="Search hashtag..."
                    type="text"
                    autoComplete="off"
                    defaultValue={this.props.tag.name}
                    onKeyPress={this.search}
                    style={{ marginBottom: 25 }}
                />
                
                {posts}
                
                {this.props.tag.hasNextPage
                    ? loadMoreButton
                    : null}
                
            </Aux>
        )
    }
}

export default Tags