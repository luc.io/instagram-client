# Instagram Client

A client for Instagram without using their official API.  
It uses scraping to get data for profiles, posts, and tags, with paging for all of them.

## The Brief

The project was to be a simplified Instagram client with two main sections:
1. An admin panel with profiles that should be
    + addable to the local system, but only if they actually exist
    + removable
    + tappable to lead to the non-admin view of the profile
    + stored in Redux
2. A user panel with a view of the profiles added to the system
    + Profiles should be tappable and load the first 24 photos, showing a loader in the meantime

## Built With

* [React](https://reactjs.org)
* [Redux](https://redux.js.org)
* [Semantic UI](https://react.semantic-ui.com)

## Features

+ Pseudo-following system
    + Follow and unfollow profiles
    + Keeps track of visited profiles, with the option to follow
    + No log in required
    + The data is stored in Local Storage so it persists between page loads and restarts, thereby acting like a real profile
+ Profile view for any username (if it exists and is not private)
    + Pagination
    + Refresh button
    + Grid and linear view of gallery
    + Tapping a post in grid view opens the view for the post with more detail
        + Post can be refreshed
        + Videos play as expected
        + Post caption has tappable hashtags and usernames
+ Search by hashtag
    + Refresh button
    + Pagination
    + Clicking post opens post view, allowing navigation to the post's profile
    
## Demo

The application is best experienced on mobile and is hosted at:
### https://ig.oak.re

## Process

### Unofficial Instagram API

I initially wanted to use Instagram's official API because it would have provided me with a clean and standard way of interacting with their data. Instead, the company has been progressively removing access to their endpoints, even with an API key. It soon become clear that the complexity of the project lay in getting data from Instagram rather than anything with React or Redux.

To begin with, when creating the admin view where one can search for a profile and add it to the system, I realised I couldn't do it with Instagram's API, so the only option was to send a `GET` request directly to `instagram.com/x` where `x` is the given username. Based on the returned status code I know if the profile actually exists. Additionally, I don't allow private profiles to be added because they have no media to display. When trying to add an existing and public profile, I store it in the Redux store along with the first 12 of its posts.

It used to be possible to request some public data without the API, by appending `/?__a=1` to a public URL, e.g. `instagram.com/cats_of_instagram/?__a=1`. This would return plain JSON which could be parsed relatively easily. In April 2018<sup>__[1]__</sup> they stopped providing access to public data other than one's own with their official API and not long after the `/?__a=1` trick stopped working as well, unless you're logged in.

The current solution is to parse the HTML of the page `instagram.com/cats_of_instagram` and find a global JSON object called `window._sharedData`. Here we find the first 12 posts, profile data, authentication codes, and other things for logging/analytics.

![alt text](readme/sharedData.png)

Highlighted are the two most important pieces of information.
1. The profile object with username, profile picture, bio, etc., and the first 12 posts
2. The `rhx_gis` hash, which we'll come back to later

At this point, we have everything we need to reproduce the Instagram profile view...

![alt text](readme/profile.png)

...but only the first 12 posts. How would we get more without the official API? It turns out that in that profile object we got from parsing the HTML of the profile page there are two golden variables:

![alt text](readme/page_info.png)

1. `end_cursor` is used as a unique identifier for the next page
2. `has_next_page` is used to determine the visibility of the Load More button

By monitoring the Network tab in our browser's developer tools we see that when scrolling down the real profile page, Instagram would send a request similar to this:
```
https://www.instagram.com/graphql/query/?query_hash=5b0222df65d7f6659c9b82246780caa7&variables=%7B%22id%22%3A%227013409%22%2C%22first%22%3A12%2C%22after%22%3A%22QVFCRTRFN3NoQXNaYUd2N3AtcDJvV1dQUy1LbDlqdGMxN19saktYdWt1ZlhsQXI2cXVlcGVzSmNjUGplMlhBMnFwMkZYazJlOFZVSzRob1JTOFpwUlhSWA%3D%3D%22%7D
```

If we break this down we see it is comprised of a number of things:
1. `query_hash` which in this case was `5b0222df65d7f6659c9b82246780caa7`
2. `variables` which when decoded is something like this:
    + `{"id":"7013409","first":12,"after":"QVFCRTRFN3NoQXNaYUd2N3AtcDJvV1dQUy1LbDlqdGMxN19saktYdWt1ZlhsQXI2cXVlcGVzSmNjUGplMlhBMnFwMkZYazJlOFZVSzRob1JTOFpwUlhSWA=="}` as a string
        + `id` is the profile ID
        + `first` determines how many posts to retrieve
        + `after` is the `end_cursor` we found above
        
Trying to access this URL in Postman results in a `403 Forbidden` response, because Instagram uses an authentication header called `X-Instagram-GIS` whenever their `/graphql/query` endpoint is hit. In the case of the above request the `X-Instagram-GIS` or `gis` was `5679a463278fcc065f5dc7a0c771d2ee`. This value changes on every query and some searching shows that the way it's generated has changed progressively.<sup>__[2]__</sup> Currently, the hash is generated as such:

```javascript
const gis = md5(rhx_gis + ':' + variables)
```

The `rhx_gis` hash we get whenever we load the profile page and the `variables` can be populated with the profile data.

The `query_hash` stays the same between queries, regardless of profile. It seemed static but I knew that it would change, having seen a discussion on the location of this hash.<sup>__[3]__</sup> It can always be found in the JavasScript file called `static/bundles/base/ProfilePageContainer.js` but the bundle id at the end can change at any time, which at the time of writing is `/ed86f768fdd0.js`. My solution is at the same time I'm parsing the profile's HTML page, I search for this filename, request the asset, then find the `query_hash`. This hash describes a specific type of request, so there is one hash for paging profiles, one for tags, one for comments etc.

With all of these variables in place, it is possible to paginate a profile.  
The same principles can be applied to loading and paginating tags.

The logic for requesting and handling data from Instagram is separated into a local module called `ig-unofficial-api` with the aim of abstracting as much of the API from the application as possible.

## Issues Encountered

### CORS
The main issue I encountered was with CORS. Having succesfully tested the query endpoint with Postman, I realised when I actually implemented it into the application that the browser was preventing me from making the request. Requesting the profile page worked as expected but the queries did not. This makes sense as the profile page is public facing and can be accessed from anywhere, but Instagram want their query endpoint to be called only from within their own application.

That was when I learned that Postman doesn't have this problem because it makes the request directly to the server without going through a browser. I found a proxy<sup>__[4]__</sup> which worked in a similar way to Postman, but allowing me to make the request from the browser. I've set up an instance on my development server.<sup>__[5]__</sup> With this proxy I simply prefix the Instagram query endpoint with `https://cors.oak.re/`.

I eventually added the proxy even to the public endpoints, because Firefox.

## Possible Improvements

+ Add view for post comments, with pagination
+ Add support for carousels and boomerangs
+ Add automatic clicking of Load More, à la infinite scrolling
+ Further decouple my `ig-unofficial-api` local module from the application and turn into a completely separate module
+ Display both top and recent posts within a tag instead of just recent
+ Refine post caption hashtag/username parser to include usernames with . symbol and other edge cases
+ Use multiple Redux reducers to separate different types of actions
+ Expand the homepage list of profiles into a news feed, but this would require requesting all the profiles in the list as individually before sorting them chronologically. Requesting the full profile page instead of a single JSON endpoint with an array of usernames means there would be a lot of overhead and it wouldn't be particularly friendly to IG's servers if we fire off 100 requests in a second.
+ Improve layout for desktops and tablets
+ Add a fallback for the Menu's `-webkit-backdrop-filter` which works on Safari, both mobile and desktop, but I couldn't get it working on any other browser
+ Remove Semantic UI's errors about incorrect icon names when 'outline' is included, even though those icons exist
+ Improve styling visually and reduce inline styles in favour of stylesheets
+ Extract repeated code into their own components
+ There's always room to refactor

## Opinion

I'm happy with the way this project turned out. Considering it uses a reverse engineered way of accessing the data, I think it does a good job of imitating the Instagram app. The application is read-only in the sense that it doesn't require (or even support) logging in, so the icons for liking, commenting, sending, and bookmarking are dummies. With the current pseudo-following system post bookmarking functionality could be implemented, but liking and commenting couldn't.

## Links
__1__ https://later.com/blog/instagram-api/  
__2__ https://gist.github.com/marcoqu/e17e1c4414f8d18e6672976d941161fa#gistcomment-2552996  
__3__ https://www.diggernaut.com/blog/how-to-scrape-pages-infinite-scroll-extracting-data-from-instagram/  
__4__ https://github.com/shalvah/cors-escape  
__5__ https://cors.oak.re